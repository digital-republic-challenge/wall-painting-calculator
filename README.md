## Calculadora de Tinta

> **Requisitos**
> - node v18.12.1
> - npm 9.6.6
.


> **Para rodar o projeto localmente, siga este passo a passo**
> - Clone o repositório com `git clone <URL>`
> - Na pasta backend, execute o comando `npm run backend`
> - Na pasta frontend, execute o comando `npm run frontend`
> - A url padrão do frontend é `http://localhost:3000`.
