import React, { FC, ReactNode } from 'react';
import './modal.sass';

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  title: string;
  children: ReactNode;
}

const Modal: FC<ModalProps> = ({ isOpen, onClose, title, children }) => {
  return isOpen ? (
    <div className="modal">
      <div className="modal-overlay" onClick={onClose} />
      <div className="modal-content">
        <div className="modal-title">{title}</div>
        {children}
        <button className="modal-close" onClick={onClose}>X</button>
      </div>
    </div>
  ) : null;
};

export default Modal;
