'use client';
import { useState } from 'react';
import './homepage.sass';
import Modal from './Modal';

export default function MyApp() {
    // Modal
    const [ready, setReady] = useState(true);
    const [isOpen, setIsOpen] = useState(false);
    function handleOpenModal() {
        setIsOpen(true);
    }
    function handleCloseModal() {
        setIsOpen(false);
    }

    const [message, setMessage] = useState("");
    const [walls, setWalls] = useState([
        {id: 0, width: 0, height: 0, frames: ['']},
        {id: 1, width: 0, height: 0, frames: ['']},
        {id: 2, width: 0, height: 0, frames: ['']},
        {id: 3, width: 0, height: 0, frames: ['']},
    ]);

    function sendData(event: any) {
        event.preventDefault();
        setReady(false);
        walls.forEach(wall => {
            wall.frames = wall.frames.filter(str => str !== '')
        });

        fetch('http://localhost:8080/calculate', {
            method: 'POST',
            mode: 'cors',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(walls)
        })
        .then(async data => {
            let json = await data.json();
            let msg = ``;
            if(data.status === 200) {
                msg = `Sugestão: `;
                Object.entries(json.message).forEach((entry) => {
                    const [key, value] = entry;
                    msg += `${value} lata(s) de ${key} Litros, `
                });
            } else if(data.status >= 400 && data.status < 500) {
                msg = `Parede ${json.id+1}: ${json.message}`;
            }
            setMessage(msg);
            handleOpenModal();
            
        })
        .catch(error => {
            let msg = "Erro no servidor";
            setMessage(msg);
            handleOpenModal();
        })
        .finally(() => setReady(true));
    }

    function countFrames(target:string, frames: string[]) {
        let counter = 0;
        for (let frame of frames.flat()) {
            if (frame == target) {
                  counter++;
              }
          };
        return counter;
    }

    function changeProperty(event: any, id: number, property: string) {
        setWalls(walls.map(wall => 
            wall.id === id ? Object.assign(wall, {[property]: event.target.value}) : wall
        ));
    }

    function addFrame(id: number, frame: string) {
        setWalls(walls.map(wall => 
            wall.id === id ? Object.assign(wall, wall.frames.push(frame)) : wall
        ));
    }

    function removeFrame(id: number, frame: string) {
        setWalls(walls.map(wall => 
            wall.id === id ? Object.assign(wall, {frames: removeFirst(wall.frames, frame)}) : wall
        ));
    }

    function removeFirst(arr: string[], pattern: string): string[] {
        const index = arr.findIndex(item => item === pattern);
        if (index !== -1) {
          arr.splice(index, 1);
        }
        return arr;
    }

    const listWalls = walls.map(wall => {
        return (
            <div className='wall' key={wall.id}>
                <p className='wall-header'>Parede {wall.id+1}</p>
                <div className='wall-panel'>
                    <div className='col'>
                        <p>Largura (m)</p>
                        <input type="number" step="0.01" min="0" placeholder='Largura (m)'
                            value={wall.width} onChange={(event) => changeProperty(event, wall.id, 'width')} required/>
                    </div>
                    <div className='col'>
                        <p>Altura (m)</p>
                        <input type="number" step="0.01" min="0" placeholder='Altura (m)'
                        value={wall.height} onChange={(event) => changeProperty(event, wall.id, 'height')} required/>
                    </div>
                    <div className='col'>
                        <p>Portas</p>
                        <input className='col-frame' type="text" placeholder={`Portas: ${countFrames('door', wall.frames)}`} disabled/>
                        <button type="button" className="button-frame" onClick={() => addFrame(wall.id, 'door')}>+</button>
                        <button type="button" className="button-frame" onClick={() => removeFrame(wall.id, 'door')}>-</button>
                            
                    </div>
                    <div className='col'>
                        <p>Janelas</p>
                        <input className='col-frame' type="text" placeholder={`Janelas: ${countFrames('window', wall.frames)}`} disabled/>
                        <button type="button" className="button-frame" onClick={() => addFrame(wall.id, 'window')}>+</button>
                        <button type="button" className="button-frame" onClick={() => removeFrame(wall.id,'window')}>-</button>
                    </div>
                </div>
            </div>
        )
    });

    return (
        <div className='container'>
            <div className="header">
                <h1>Calculadora de Tinta</h1>
            </div>
            <div className='calculator'>
                <form onSubmit={sendData}>
                    {listWalls}
                    <div className='send-form'>
                        <button type='submit' disabled={!ready}>Calcular</button>
                    </div>
                </form>
            </div>
            <Modal isOpen={isOpen} title="" onClose={handleCloseModal}>
                <p>{message}</p>
            </Modal>
        </div>
    );
}