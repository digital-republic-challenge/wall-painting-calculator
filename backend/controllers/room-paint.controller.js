const Room = require('../models/room.model');
const paintCans = require('../models/paintCans.model');
const { CustomException } = require('../exceptions/Exceptions');

function runCalculator(req, res, next) {
    const _walls = req.body;
    const room = new Room(_walls);
    try {
        room.validate();
        const r = calculatePaintCans(room.getPaintableArea());
        return res.status(200).send({"message": r});
    } catch(error) {
        if(error instanceof CustomException) {
            console.log(error.name);
            return res.status(400).send({"id": error.id, "message": error.message});
        }
        else {
            console.log(error);
            return res.status(500);
        }
    }
}

function calculatePaintCans(roomArea) {
    const totalLiters = roomArea / 5;
    let litersRemaining = totalLiters;
    var totalPaintCans = {};

    for(let i=0;i < paintCans.length; i++) {
        while(litersRemaining >= paintCans[i]) {
            litersRemaining -= paintCans[i];
            totalPaintCans[paintCans[i]] === undefined 
              ? totalPaintCans[paintCans[i]] = 1
              : totalPaintCans[paintCans[i]]++;
        }
    }

    // Check if it has less than 0.5 liters remaining, if so,
    // we add the smallest can.
    if(litersRemaining > 0 && litersRemaining < paintCans[paintCans.length-1]) {
      let minCan = paintCans[paintCans.length-1];
      totalPaintCans[minCan] === undefined 
              ? totalPaintCans[minCan] = 1
              : totalPaintCans[minCan]++;
      litersRemaining = 0;
    }
    return totalPaintCans;
}

module.exports = {calculatePaintCans, runCalculator}