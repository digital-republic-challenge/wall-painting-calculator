const express = require('express');
require('dotenv-safe').config();
const app = express();

const port = process.env.PORT;

app.listen(port, (err, callback) => {
    if(err) {
        console.log(err);
    }

    console.log(`App listening on port ${port}`);
});

module.exports = app;