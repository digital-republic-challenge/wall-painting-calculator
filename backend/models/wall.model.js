const { InvalidAreaException, ThirtyPercentDoorException, FifthPercentRuleException } = require('../exceptions/Exceptions');
const Frame = require('./frame.model');

module.exports = class Wall {
    constructor(obj) {
        // Maximum area frames can occupy from total area (in %)
        this.wallFrameMaxRate = .50;
        this.minSpaceBetweenDoorAndWall = .30;

        this.id = obj.id;
        this.width = obj.width;
        this.height = obj.height;
        this.frames = obj.frames;
    }

    validate() {
        let area = this.getArea();
        // Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, 
        // mas podem possuir alturas e larguras diferentes
        if(area < 1 || area > 50) {
            throw new InvalidAreaException(this.id, "Área das paredes devem estar entre 1m² e 50m²");
        }

        if(this.frames) {
            for(let item of this.frames) {
                // A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
                let frame = Frame[item];
                if(item === 'door') {
                    if((this.height - frame.height) < this.minSpaceBetweenDoorAndWall) {
                        throw new ThirtyPercentDoorException(this.id, "A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta");
                    }
                }
            }
        }

        // O total de área das portas e janelas deve ser no máximo 50% da área de parede
        let framesArea = this.getFramesArea();
        if((framesArea / area) > this.wallFrameMaxRate) {
            throw new FifthPercentRuleException(this.id, "Esquadros não podem ocupar mais de 50% das paredes");
        }

    }

    getArea() {
        return this.width * this.height;
    }

    getFramesArea() {
        if(this.frames) {
            let totalFramesArea = 0;
            this.frames.forEach(item => {
                let frame = Frame[item];
                totalFramesArea += (frame.width * frame.height);
            });
            return totalFramesArea;
        }
        return 0;
    }
}