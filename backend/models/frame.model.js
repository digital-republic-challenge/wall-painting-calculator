const frame = {
    'door': {
        width: 0.80,
        height: 1.90
    },
    'window': {
        width: 2.00,
        height: 1.20
    }
}

module.exports = frame;
