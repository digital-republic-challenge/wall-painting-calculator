const { FifthPercentRuleException } = require('../exceptions/Exceptions');
const Wall = require('../models/wall.model');

module.exports = class Room {
    constructor(walls) {
        this.walls = this.parseWallsJson(walls);
    }

    parseWallsJson(_walls) {
        let walls = [];
        _walls.forEach(wall => walls.push(new Wall(wall)));
        return walls;
    }

    validate() {
        // Should validate all room walls
        this.walls.forEach(wall => wall.validate());
    }

    printRoom() {
        console.log(`Room Walls: ${this.walls.length}`);
        this.walls.forEach(element => {
            element.printWall();
        });
    }

    getTotalWallsArea() {
        let wallsArea = 0;

        this.walls.forEach(wall => {
            wallsArea += wall.getArea();
        });

        return wallsArea;
    }

    getTotalFramesArea() {
        let framesArea = 0;
        this.walls.forEach(wall => {
            framesArea += wall.getFramesArea();
        });

        return framesArea;
    }

    getPaintableArea() {
        return this.getTotalWallsArea() - this.getTotalFramesArea();
    }
}
