const router = require('express').Router();
const roomPaintController = require('../controllers/room-paint.controller');

router.post('/', roomPaintController.runCalculator);

module.exports = router;