class CustomException extends Error {
    constructor(message) {
        super(message);
        this.name = "Custom Exception";
        this.message = message;
        
    }
}

class InvalidAreaException extends CustomException {
    constructor(id, message) {
        super(message);
        this.name = "Invalid Area Exception";
        this.message = message;
        this.id = id;
    }
}

class FifthPercentRuleException extends CustomException {
    constructor(id, message) {
        super(message);
        this.name = "Fifth Percent Rule Exception";
        this.message = message;
        this.id = id;
    }
}

class ThirtyPercentDoorException extends CustomException {
    constructor(id, message) {
        super(message);
        this.name = "Thirty Percent Door Exception";
        this.message = message;
        this.id = id;
    }
}

class InvalidBodyFormat extends CustomException {
    constructor(id, message) {
        super(message);
        this.name = "Invalid Body Format";
        this.message = message;
        this.id = id;
    }
}

module.exports = {
    InvalidAreaException,
    FifthPercentRuleException,
    ThirtyPercentDoorException,
    InvalidBodyFormat,
    CustomException
}