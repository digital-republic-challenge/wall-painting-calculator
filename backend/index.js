const app = require('./server');
const bodyParser = require('body-parser');
const cors = require('cors');

// CORS
var allowlist = ['http://localhost:3000']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (allowlist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response
  } else {
    corsOptions = { origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}

// MIDDLEWARES
app.use(bodyParser.json());
app.use(cors(corsOptionsDelegate));

// API Routes
const roomPaintRoutes = require('./routes/room-paint.routes');
app.use('/calculate', roomPaintRoutes);

module.exports = app;