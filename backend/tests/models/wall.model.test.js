const { InvalidAreaException, ThirtyPercentDoorException, FifthPercentRuleException } = require('../../exceptions/Exceptions');
const Wall = require('../../models/wall.model');

test('it should return correct wall area', () => {
    const _wall = {width: 5, height: 4};
    const wall = new Wall(_wall);
    wall.validate();
    expect(wall.getArea()).toBe(20);
});

test('it should return correct wall and frames area', () => {
    const _wall = {width: 5, height: 4, frames: ['door', 'window']};
    const wall = new Wall(_wall);
    wall.validate();
    expect(wall.getArea()).toBe(20);
    expect(wall.getFramesArea()).toBe(3.92);
});

test('it should throw Invalid Area Exception by large Area', () => {
    const _wall = {width: 11, height: 5};
    const wall = new Wall(_wall);

    expect(() => wall.validate()).toThrow(InvalidAreaException);
});

test('it should throw Invalid Area Exception by small area', () => {
    const _wall = {width: 0.5, height: 1};
    const wall = new Wall(_wall);

    expect(() => wall.validate()).toThrow(InvalidAreaException);
});


test('It should throw Thirty Percent Rule Exception', () => {
    const _wall = {width: 3, height: 2.19, frames: ['door']};
    const wall = new Wall(_wall);
    expect(() => wall.validate()).toThrow(ThirtyPercentDoorException);
});


test('It should throw Fifth Percent Rule Exception', () => {
    const _wall = {width: 1, height: 1, frames: ['window']};
    const wall = new Wall(_wall);
    expect(() => wall.validate()).toThrow(FifthPercentRuleException);
});