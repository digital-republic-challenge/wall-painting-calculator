const Room = require('../../models/room.model');

test('it should return correctly area of painting', () => {
    const _walls = [
        {width: 4, height: 6},
        {width: 4, height: 5},
        {width: 7, height: 6},
        {width: 8, height: 5}
    ];
    const room = new Room(_walls);
    room.validate();
    expect(room.getTotalWallsArea()).toBe(126);
    expect(room.getTotalFramesArea()).toBe(0);
    expect(room.getPaintableArea()).toBe(126);
});

test('it should return correctly area of painting with frames', () => {
    const _walls = [
        {width: 4, height: 6, frames: ['door', 'window']},
        {width: 4, height: 5, frames: ['window']},
        {width: 7, height: 6},
        {width: 8, height: 5}
    ];
    const room = new Room(_walls);
    room.validate();
    expect(room.getTotalWallsArea()).toBe(126);
    expect(room.getTotalFramesArea()).toBe(6.32);
    expect(room.getPaintableArea()).toBe(119.68);
});