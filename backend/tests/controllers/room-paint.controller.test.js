const controller = require('../../controllers/room-paint.controller');
const request = require('supertest');
const app = require('../../index');

test('it should suggest correctly numbers of paint cans', () => {
    expect(controller.calculatePaintCans(95)).toStrictEqual({'18': 1, '0.5': 2});
    expect(controller.calculatePaintCans(175)).toStrictEqual({'18': 1, '3.6': 4, '2.5': 1, '0.5': 1});
    expect(controller.calculatePaintCans(300)).toStrictEqual({'18': 3, '3.6': 1, '0.5': 5});
    expect(controller.calculatePaintCans(60)).toStrictEqual({'3.6': 3, '0.5': 3});
});

test('it should return correctly paint cans', async () => {
    const response = await request(app)
        .post('/calculate')
        .send([
            {
                width: 6,
                height: 4
            }
        ]);
    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({"message": {'3.6': 1, '0.5': 3}});
});

test('it should return correctly paint cans with frames', async() => {
    const _walls = [
        {width: 4, height: 6, frames: ['door', 'window']},
        {width: 4, height: 5, frames: ['window']},
        {width: 7, height: 6},
        {width: 8, height: 5}
    ];

    const response = await request(app)
        .post('/calculate')
        .send(_walls);
    
    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({"message": {'18': 1, '3.6': 1, '0.5': 5}});
});


test('It should return correct paint cans', async() => {
    const _walls = [
        {width: 2.2, height: 6.8, frames: ['door', 'window']},
        {width: 3.4, height: 7.2, frames: ['door', 'window', 'window']},
        {width: 8.2, height: 3.2, frames: ['door', 'window', 'door', 'window']},
        {width: 4.4, height: 3.3, frames: ['door', 'window']},
    ];

    const response = await request(app)
        .post('/calculate')
        .send(_walls);
    
    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual({"message": {'0.5': 2, '3.6': 3}});
});